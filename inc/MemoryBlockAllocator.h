#ifndef TAZER_MEMORY_BLOCKALLOCATOR_H

#include <unordered_map>
#include <deque>
#include <iostream>
#include "LRUPolicy.h"

//Region-based BlockAllocator manages several cache block regions allocated in main memory
//We assume that all blocks belong to the same file
//It supports scaling up by allocating a new non-contiguous memory region

//TODO 3: Create SharedBlock Allocator which keeps one BlockAllocator per file and allows memory region exchanges
class MemoryBlockAllocator{

public:
    struct Free{
        uint32_t blockStart;
        uint32_t blockEnd;
        uint32_t regionId;
    };

    struct RegionInfo{
        bool eraseFlag = false; //If flag is set, region will be deleted after it gets empty
        uint32_t numFree; // Number of free blocks
    };

    struct Region{
        uint32_t totalBlocks;
        uint8_t *blocks; //Link to memory space
    };

    MemoryBlockAllocator(LRUPolicy *policy, uint32_t maxBlocks, uint32_t blockSize) :
                                        policy_(policy), maxBlocks_(maxBlocks), blockSize_(blockSize){};


    void allocateRegion(uint32_t numBlocks) { //TODO: consider realloc at some point?
        size_t numBytes = numBlocks*blockSize_;
        uint32_t regionId = regions_.size();
        Region newRegion = Region{numBlocks,new uint8_t[numBytes]};
        regions_[regionId] = newRegion;
        regionInfo_[regionId] = RegionInfo{false,numBlocks};
        freeBlockPool_.emplace_back( Free{nextBlockID_, nextBlockID_+newRegion.totalBlocks, regionId} ); //Add a range of free blocks
        nextBlockID_ += newRegion.totalBlocks+1;
    }

    //Performs Lazy deallocation. It just marks region to be erased and sets next nextBlockID_ to last region
    void lazyRemoveLastRegion() {
        uint32_t regionId = freeBlockPool_.size()-1;
        lazyRemoveRegion(regionId);
    }

    void lazyRemoveRegion(uint32_t regionId){
        regionInfo_[regionId].eraseFlag = true;
        if (regionInfo_[regionId].numFree == regions_[regionId].totalBlocks) {
            std::cout << "Region " <<regionId << "removed successfully" <<std::endl;

            regionInfo_.erase(regionId);
            delete[] regions_[regionId].blocks;
            regions_.erase(regionId);
        }
    }

    Region* stealRegion(uint32_t regionId) { //TODO: Need to think this one

    }

    int addRegion(Region * region) {
        uint32_t regionId = regions_.size();
        regions_[regionId] = *region;
        regionInfo_[regionId] = RegionInfo{false,region->totalBlocks};
        freeBlockPool_.emplace_back( Free{nextBlockID_, nextBlockID_+region->totalBlocks, regionId} ); //Add a range of free blocks
        nextBlockID_ += region->totalBlocks+1;
    }


    uint32_t getFreeBlock(uint32_t fileBlockId) {
        //First check in pool of free blocks
        uint32_t selectedBlock = -1;
        while (!freeBlockPool_.empty()) {
            Free free = freeBlockPool_.front();
            if (regionInfo_[free.regionId].eraseFlag) {
                freeBlockPool_.pop_front();
            } else {
                selectedBlock = free.blockStart;
                free.blockStart++;
                regionInfo_[free.regionId].numFree++;
                if (free.blockStart>free.blockEnd) {
                    freeBlockPool_.pop_front();
                }
                return selectedBlock;
            }
        }
        //Else, replace LRU block
        if (policy_) {
            selectedBlock = policy_->getLRU(); //TODO: Rewrite block data
            if (selectedBlock > -1) {
                return selectedBlock;
            }
        }

        //No space left consider adding a new region or requesting region from other Allocator //TODO: To be implemented in SharedBlockAllocator
        return selectedBlock;

    }

    int releaseBlock(uint32_t blockID) {
        uint32_t regionId = policy_->remove(blockID); //Update LRU
        if (!regionInfo_[regionId].eraseFlag) {
            freeBlockPool_.emplace_back(Free{blockID, blockID,regionId}); //TODO: Need to figure out how to remove previous free regions?
        } else {
            lazyRemoveRegion(regionId);
        }

        return 0;
    }

private:
    std::unordered_map<uint32_t, Region> regions_; //List of allocated memory regions
    std::unordered_map<uint32_t, RegionInfo> regionInfo_; //Stats associated to each region
    std::deque<Free> freeBlockPool_; //Pool of free block ranges
    uint32_t nextBlockID_ = 0;
    uint64_t blockSize_;
    uint32_t maxBlocks_; //Maximum number of blocks allowed for this block allocator. It can be used to limit file capacity. Might want to move it to SharedBlockAllocator
    uint32_t numBlocks_;
    LRUPolicy *policy_; //Block replacement policy

};

#define TAZER_MEMORY_BLOCKALLOCATOR_H

#endif //TAZER_MEMORY_BLOCKALLOCATOR_H
