#ifndef TAZER_BITBUCKET_LRUPOLICY_H

#include<unordered_map>

// LRU Replacement Policy
// Implemented with a Map + Doubly Linkedlist
//TODO 1: Create source file
//TODO 2: Create abstract class Policy
class LRUPolicy {
public:

    LRUPolicy(uint64_t capacity) {
        maxcap = capacity;
        first = last = nullptr;
    }

    uint32_t getLRU() {
        if (map.empty() || last == nullptr) return -1;
        auto *selected = last;
        if (last->prev) {
            last = last->prev;
            last->next = nullptr;
        } else {
            first = last = nullptr;
        }
        map.erase(selected->key);

        return selected->val;
    }

    int remove(uint32_t key) {
        if (map.find(key) == map.end()) return -1; //Not found
        Node *n = map[key];
        if (n->prev) {
            n->prev->next = n->next;
        }
        if (n->next) {
            n->next->prev = n->prev;
        }
        if (first == n) {
            first = n->next;
        }
        if (last == n) {
            last = n->prev;
        }
        map.erase(key);
        return 0;
    }

    int get(uint32_t key) {
        if (map.find(key) == map.end()) return -1; //Not found

        Node *n = map[key];

        //If not already last
        if (n != last) {
            //Remove it
            if (n == first) {
                first = n->next;
                n->next->prev = nullptr;
            } else {
                n->prev->next = n->next;
                n->next->prev = n->prev;
            }
            //and Put it at the end of the list
            last->next = n;
            n->prev = last;
            n->next = nullptr;
            last = n;
        }

        return n->val;

    }

    void put(uint32_t key, uint32_t value) {
        //TODO: move to front?
        if (get(key) != -1) {
            map[key]->val = value;
            return;
        }

        Node *n = nullptr;
        if (map.size() == maxcap) {//We need to evict
            //Remove first
            n = first;
            map.erase(n->key);
            first = n->next;
        }
        if (n == nullptr) {
            n = new Node();
        }
        n->key = key;
        n->val = value;

        map[key] = n;

        //Add to last in the list
        if (last == nullptr) {
            last = first = n;
        } else {
            last->next = n;
            n->prev = last;
            n->next = nullptr;
            last = n;
        }

    }

private:
    struct Node{
        uint32_t key,val;
        Node *next, *prev;
    };

    uint64_t maxcap;
    std::unordered_map<uint32_t,Node*> map;
    Node *first, *last;
};

#define TAZER_BITBUCKET_LRUPOLICY_H

#endif //TAZER_BITBUCKET_LRUPOLICY_H
