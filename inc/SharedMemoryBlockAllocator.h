#ifndef TAZER_MEMORY_BLOCKALLOCATOR_H

#include <unordered_map>
#include <deque>
#include <iostream>
#include "LRUPolicy.h"
#include "MemoryBlockAllocator.h"

//SharedBlock Allocator keeps one allocator per file
//It extends the functionality by allowing exchanges of regions among files (allocators)

//TODO 1: Implement add/remove regions
//TODO 2: Policy to select target file/allocator to "steal" region from
class SharedBlockAllocator{
private:
    std::unordered_map<uint32_t,MemoryBlockAllocator*> allocators_;

public:
    SharedBlockAllocator(){}

    int addFile(uint32_t fileId, uint32_t maxBlocks, uint32_t blockSize){
        if (allocators_.find(fileId) == allocators_.end()) return -1; //File already exists

        allocators_[fileId] = new MemoryBlockAllocator(new LRUPolicy(maxBlocks), maxBlocks, blockSize);
    }

    int removeFile(uint32_t fileId) {
        if (allocators_.find(fileId) == allocators_.end()) return -1; //File already removed
        delete allocators_[fileId];
        allocators_.erase(fileId);
    }

    int addRegion(uint32_t fileId, uint32_t numBlocks) {
        if (allocators_.find(fileId) == allocators_.end()) return -1; //File does not exist
        allocators_[fileId]->allocateRegion(numBlocks);
        return 0;
    }

    int addRegion(uint32_t fileId, MemoryBlockAllocator::Region *region){
        if (allocators_.find(fileId) == allocators_.end()) return -1; //File does not exist
        allocators_[fileId]->addRegion(region);
    }

    int removeRegion(uint32_t fileId, uint32_t regionId = -1) {
        if (allocators_.find(fileId) == allocators_.end()) return -1; //File does not exist
        allocators_[fileId]->lazyRemoveRegion(regionId);
        return 0;
    }


    uint32_t getFreeBlock(uint32_t fileId, uint32_t blockId) {
        if (allocators_.find(fileId) == allocators_.end()) return -1; //File removed
        return allocators_[fileId]->getFreeBlock(blockId);
    }
    int releaseBlock(uint32_t fileId, uint32_t blockId) {
        if (allocators_.find(fileId) == allocators_.end()) return -1; //File removed
        return allocators_[fileId]->releaseBlock(blockId);
    }

    MemoryBlockAllocator::Region* stealRegion(uint32_t fileId){
        if (allocators_.find(fileId) == allocators_.end()) return nullptr; //File removed
        return allocators_[fileId]->stealRegion(0); //TODO: which one?
    }

};

#define TAZER_MEMORY_BLOCKALLOCATOR_H

#endif //TAZER_MEMORY_BLOCKALLOCATOR_H
