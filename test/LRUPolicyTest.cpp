#include <assert.h>
#include "LRUPolicy.h"

int main(int argc, char *argv[]) {
    LRUPolicy policy(2);

    policy.put(1, 1);
    policy.put(2, 2);
    assert(policy.get(1) == 1);       // returns 1

    policy.put(3, 3);    // evicts key 2

    assert(policy.get(2) == -1);       // returns -1 (not found)

    policy.put(4, 4);    // evicts key 1
    assert(policy.get(1) == -1);       // returns -1 (not found)
    assert(policy.get(3) == 3);       // returns 3
    assert(policy.get(4) == 4);       // returns 4
    return 0;
}
