#include <assert.h>
#include "LRUPolicy.h"
#include "SharedMemoryBlockAllocator.h"

#define BLOCK_SIZE 1
#define MAX_BLOCKS 10
int main(int argc, char *argv[]) {
    LRUPolicy policy(MAX_BLOCKS);
    SharedBlockAllocator mem;

    mem.addFile(1, MAX_BLOCKS, BLOCK_SIZE);


    return 0;
}
