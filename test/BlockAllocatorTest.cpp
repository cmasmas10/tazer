#include <assert.h>
#include "LRUPolicy.h"
#include "MemoryBlockAllocator.h"

#define BLOCK_SIZE 1
#define MAX_BLOCKS 10
int main(int argc, char *argv[]) {
    LRUPolicy policy(MAX_BLOCKS);
    MemoryBlockAllocator mem(&policy, MAX_BLOCKS, BLOCK_SIZE);

    //Create 3 regions

    //Allocate

    //Remove region 2

    //Allocate
    //Allocate
    //Allocate

    //Create 2 extra regions

    //Allocate
    //Allocate

    return 0;
}
